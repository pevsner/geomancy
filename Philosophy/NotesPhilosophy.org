#+TITLE:PHILOSOPHY
* My Notes / Ideas
** You become what you do
   Continually bend one way, you end up bent that way.
   Environmental factors:  like a tree bending away from a harsh
   prevailing wind.
*** Existentialist : "man is a becoming"
** Ice is cold, minimal jiggle, not enough energy to roll
   over each molecule. Like oranges/apples in a carton tray.
   Slippery is the heat of the shoe on the surface melting the ice
   creating the slip.
** Blue Earth energy is slow, less jiggly
   Meanders, like slow water (sludgy?). Ware molecules like to stay
   together, keep fighting to get in with each other.
   Thus at the boundary you get surface tension where they are all fighting
   to get in.
** Red Heavenly energy is fast: more jiggly
   Fast moving, like fast moving air, even faster, atoms in frenzy.
   Fire is Carbon and oxygen snapping together when close enough (repel at a
   distance. When a frenzy of activity happens with initial heat gets it going.
   You get fire. Massive Co2 comes off. Fire is the sun energy inherent
   in the substance of the tree coming out. The heat and light is
   sunshine released.
   SUN is thermonuclear energy. * expand
** Steam Qi is he meeting of the two? elaborate
   Steam is: heated water, molecules accelerated, they bounce move to fast
   they can't attach to each other, so are released as steam.
** TIME : If you are doing something you will never ever stop doing
   with a specific aim. Then with relative time, why not just
   pull the result back to where you are.
** Could it be that block universe combined with Multi(bubble) is it!
* Thought is electromagnetic. The body completely changes it's material.
  I think.....therefore I am (Descartes).
  THINK to change growth, form, shape?
  A thought is an electromagnetic impulse. (what is the form or nature
  of each thought (+,+,-,-,-,+,+ = a thought)?
  The bodies components/material changes. Time, what is it actually?
  So it should be possible to re-generate completely?
  A tree, shaped by environment (eg.bends with prevailing wind)
  > Nature wants to achieve the best it can, is this in every field. eg
  virtue, knowledge, enlightenment.
  > Does Man naturally want to become enlightened and change form into
  a more advanced being. What is that advanced being.
  > Is everything predetermined: as in block theory.
  A tree grows from the air rather than from out of the ground. Carbon, the
  heat of the sun accelerates the molecules to snap C and O2 together,
  Stored CO2.
  You could theoretically "think, imagine" your atoms accelerating to increase your temperature.
  > Is a fever just the activity of the body working overly hard at fighting
  that gives off that additional heat?
* Socrates, Plato

** Justice
   Socrates on justice.
   His question to a trader "what is justice?"
   Answered: Justice is truth and paying ones debts.
   Replies: What if a man gives you a weapon and comes
   to recover the debt, but is not of sound mind.
   Therefore it would be just not to return the weapon.
** God & Spirit.
*** Is it true that some have inherent knowledge.
    Where has this knowledge come from?
    Therefore having prior knowledge implies there was a
    former existence. QED. The soul is immortal and survives
    death.
** Moral relativism
** Cultural relativism
** Allegory
   Rather than a Fable.
   A story whereby the reader comes to their own conclusions.
   Finishes the story
*** The Cave Allegory
**** People are chained in a pitch-black cave to face forward from the opening.
**** There is a fire, which they can't see, behind them casting light forward.
**** There is a platform in front of them with puppets and objects, as actors,
     which are moved, casting shadows on the wall behind, although in front of the
     prisoners.
**** This is the prisoners only reality. The illusion, the "show".
     TV is the modern day equivalent. Although Plato really means the everyday general illusion.
**** What if someone manages to break free of the chains and discovers the reality.
     They can see the fire and the shadows cast. What if they fumble to the cave tunnel opening
     and proceeds to the outside. They will see a new reality. The reality of the sun illuminating
     everything. The vastness of the outside, compared to the small, limited and fake experience
     of the cave.
     If he returns to explain the situation to the chained prisoners they will disbelieve him
     ridicule his stupidity and maybe inflict harm because it is so removed from their reality.
     Plato suggests that the man returning is like a Philosopher or Guru seer.
     Osho (ref) suggests that if you do get out of the cave, keep on going, as you only waste
     your energy trying to convince the prisoners. (even if in this allegory they are chained)
     he suggests they will want to kill you.
     *My Take* Maybe, go back inside and unchain one. Then leave.
*** The Cave on the divided line
**** 1. The shadows as illusions
**** 2. The babble of the prisoners
        Common sense based on illusion.
**** 3. Outside the cave, seeing truth.
**** 4. Finding the truth, philosophising on true observation.
** Dialectic
   A form of reasoning based upon a dialogue of arguments
   and counter arguments. Advocating propositions and
   counter propositions.

** Divided Line of knowledge
   divided into 4 variable parts
*** Basic division.
    Knowledge (in the intelligible world)
    Objects (in the visible world.
    The Ladder of knowledge.
*** 1. Conjecture or imagining. Lowest level of knowledge.
    Mental activity at a minimum. Awareness of shadows    (cave). Optical illusions, dreams. Fantasies. Artistic images are copies of real objects. Fabricators of illusions. Shadow knowledge, TV.
*** 2. Belief, perception of actual objects.
    Recognising physical, visible things.
    Classification of actual things.
    Does not recognise deeper knowledge, eg. abstract truths. Like a Botanist. Sensory perception can not give true knowledge. Is always subject to change, as everything is in flux. We can never be sure as it is based on our perception. It is only opinion, it does however provide rough estimations. The senses only give concrete observation based knowledge.
*** 3. Rational understanding or intellect.
    Scientific analysis


. eg Botanist knowing species DNA etc. Ascending from belief to rational understanding.

   Does the state exist to serve the individual, or, does the individual exist to serve the state.
   What is justice in the state.
   The justice of the city is the same as for the individual. The state has 3 parts
   *Producer class.* Appetite rules, money & what money can buy.
   *Military warrior class.* Spirited. Lives for honour.
   *Ruling class.* Reason.

   The Protagoras.
   Conclusion about democracy.
   If you are ill you consult a Doctor.
   You don't consult the ignorant many.
   A skill requires specialised and intensive training well beyond that for say a cobbler, ship builder.
   Selection of the ruling class.
   Hereditary, children of the most intelligence likely to be the kids of highest intelligence. Children of shoemakers have the natural capacity to only be shoemakers.
   Genetics limitations. Therefor all children kept under testing to see if they can be suitable.
   Gender. Men beget & Women bear children.
   Dialectic: Being able to reason through dialogue, argument and counter argument.
   The Ruling classes should own no private property,
   money, should live like the military ( common barracks, sleeping quarters, clothing, food moderate qualities, no family life to avoid loyalty conflicts, sexual activity to be limited "sacred marriages" to restrict activity to just hereditary reproduction. Like breeding thoroughbreds.
   Children educated with special facilities.
   Children with mental or physical defects should be put to death.

   The *"Producer"* class , the many, love getting and spending. Plato planned regulation on the accumulation of wealth to prevent the clever getting too rich. And thus cause conflict, envy. Must be given the education "to be virtuous producers", fables, censorship, Arts & Music.
   Violence in the Arts should be censored, as it makes it appear commonplace and acceptable.

   Plato advocates the subordination of the people to the state. Rejected individual and democracy.
   He would have detested that "might/strength" makes "right".
   His one justification : founded on true knowledge. Any other justification for government power, race, glorification or leader or social class.

   Western democracies have, supposedly "checks and balances to counter imbalance of power and corruption.
*** There are three types of being.
    Each wishing to fulfil their role.
    Including Freud's interpretation.
   1. Reason: Knowledge and truth. (Freud splits into two parts 1. Ego: Reasons functions of perception and intelligence. 2. Super Ego: Reason functions of moral judgements of knowledge of the good.
   2. Spirited: Success and public acclaim. (Freud demotes aggression to a "drive or instinct").
   3. Appetites: Only for money. (Freud ID, bodily, sexual appetites, drives).
      NB. The elements of reason are no longer master, but mediator between the ID and Super Ego. The conflicts of the ID (desire) and the Super Ego (moralising of the ID's behaviour). Like the Super Ego (rider) trying to hold in check the ID (wild uncontrollable horse).
*** Bodily - appetites, sex, indulgences.
*** Pure Rational -speech and Reason
*** Moral good. making sense, philosophising on the above.
   The idea is living up to ones own nature.
   Ones nature consists of all of the above.
   Therefore to pursue only indulgences denies the other aspects.
   The *good life* is the *balanced* life.
   Balancing a bodily healthy life, a thinking rational
   life and philosophising.
   Reason, appetites and spirit.
** Virtue.
   The right conduct of life.
   Action that flows from knowledge.
**** The tripartite soul
**** All the forms
**** Ideas of the good.
** Classes in Society.
*** Producer class.
    Bodily appetites are dominant.
    Live only for money and things money can by.
*** Military Class
    Honour, Spirited element.
*** Ruling Class
    Reason & order.
**
* Aristotle
  Studied under Plato for 20yrs, even taught Alexandra The Great. Only 1/5th of his works remain.
Mostly just his lecture notes.
  The first works that came in the form he wrote.
** Ground he covered.
Sciences, especially Biology, Metaphysics, life and mental facilities, Ethics and Political theory, Literature & Rhetoric.
** Flexible and open ended approach.
** Unifying approach.
   He looked at what we see and what we say, then return to analyse.
Begin by:
Setting down "the Appearances"
working through the puzzles
Coming back to them and saving the greatest number and the most basic.
Eg. Philosopher working on time:
Setting down our perceptual experience of time and duration.
& our ordinary beliefs of time and what we say.
Set it down see if there are any contradictions.
When you find contradictions your go to work sifting  through sorting. Find which are basic, preserve those and discard the conflicting ones. So you come back to ordinary discourse with increased structure and understanding.
** Time
   : Any distinction between the world and our discourse about the world. What we perceive(see) and what we say.
   He made use of our perception of the world and our  ordinary sayings and beliefs about the world.
   Is his approach too pedestrian?
   Confined to the surface of experience, rather than
   the underlying deeper level, like Plato?
   He would say:
   Our experience is full of wonder, beauty and richness.
   We never can go beyond our experience.
   Therefore all we can do, is the mapping, investigating.
   THE PRINCIPLE of NON CONTRADICTION.
   He Argues: That contradictory properties can't apply to the same subject or object in the same time.
   EG. A woman's dress cannot be both blue and not blue at the same time and in the same place.
   Basic principle.
   He Argues to someone who argues against that assertion: that in asserting something definite, some (contradictions) things will have been ruled out.The contradictory of what he was arguing about.


* Epictetus
  Stoic Roman Period.
  Although he was from Turkey, slave for a period.
  How to live virtuously.
  Like: in the presence of less enlightened (including especially the
  affluent and regal) keep quiet and don't engage, as this will only
  pull you down.
  "Like a sheep spewing up grass to show the farmer how good he was
  and how much he had eaten. Better to reveal the amount eaten
  by growing a thick and lush fleece of wool" (something to that effect).
* Marx
  Global appeal explained:? Proletariat
  Based on Hegel Philosophy.
  God exists in the consciousness of humans, in science and philosophy.
  Hegelian's came to the conclusion that God that exists only in human consciousness
  does not exist at all. Therefore were atheists & therefore Man is God.

  The ultimate revolution cannot be purely intellectual, it must be from the industrial class, the Proletariat.
  Three fronts of change.
  1. Intellectual Criticism of Laws, Political thought and religion.
  2. Industrial working class. Proletariat.
  3. Man is god: Not yet realised, therefore what must be a world wide revolution.
     So that man can live as God.
     A huge world catastrophe so that the establishment can be overthrown.
     Destroying so.
     Therefore the structure of the world can be reorganised/ reconstructed.
     The Divinity of man rather than God.

  Reality is primarily material, not spiritual.
  Marx decided to take Hegel's dialectic to the life of Human beings in the concrete material World.

  Marx believed it was naive to think that the world could be changed through the "Age of Enlightenment".
  Significance of the Industrial Revolution, what is the future. Can inequities remain without a revolution.

  Manifesto of the Communist Party.
  Moved to London, wrote for planned the world revolution. Which happened but he didn't see.
  His writings in Paris, were never released.

* Freud
* Bertrand Russell
  Philosophy for the good should be based on kindly feelings.
  eg. Marx is an example of bad motivation.
  As his philosophy was based on hurting the bourgeoisie rather than
  helping the Proletariat

* Existentialism
  MAN IS A BECOMING!
  Basically: there is stuff, only stuff, nothing ethereal
  and man differs from all the other stuff by being "a becoming"
  through freedom. There is no point, it just exists.
  *NB* Really, a stupid and pointless exercise in intellectual masturbation.
  Main proponents: Jean Paul Sartre & Simone de Beauvoir.
  The individual defined by his "being".
  Basically opposed to the idea of ethereal influences (consciousness
  spirit, soul ).
  My existence, her existence, a dogs, a tree's, a house's existence.
  Being it, in it!
  Man has no fixed reality. He never IS, he is ABOUT to BE.
  He is living a projection into the future of what he is about to be.
  Essence: A thing (other than man) has it's essence in it.
           Man develops his own essence. He has free will to develop it.
           Mans essence is "freedom". He determines what to make of life.
           This essence is similar to Aristotle, but not!
