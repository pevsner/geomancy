# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/SHARE/doc/bash/examples/startup-files (in the package bash-doc)
# for examples
# If not running interactively, don't do anything
[ -z "$PS1" ] && return
# set -o vi #vim keys
stty -ixon # Disable ctrl-s and ctrl-q.
shopt -s autocd #Allows you to cd into directory merely by typing the directory name.
# don't put duplicate lines in the history. See bash(1) for more options
# ... or force ignoredups and ignorespace
HISTCONTROL=ignoredups:ignorespace
# append to the history file, don't overwrite it
shopt -s histappend
# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000
# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize
# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"
# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi
# PROMPT set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color) color_prompt=yes;;
esac
# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
    # We have color support; assume it's compliant with Ecma-48
    # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
    # a case would tend to support setf rather than setaf.)
    color_prompt=yes
    else
    color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
#    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt
# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac
# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi
# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# SOURCES /etc/bash.bashrc).
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    . /etc/bash_completion
fi
#   B A S H  as manpager
# export MANPAGER='vim -c "%! col -b" -c "set ft=man nomod nolist ignorecase" -'
export NCURSES_NO_UTF8_ACS=1
######## Start up #############
alias xi='xinit'
alias free='free -h'
alias vxi='vi ~/.xinitrc'
alias xdwm='xinit /usr/local/bin/dwm'
alias xbox='xinit /usr/bin/openbox'
alias calc='bc -l'
alias start='startup.sh'
alias al='alias | less'
alias alsa='alsamixer'
alias backup='./bin/backup'
alias bk='./bin/backup'
alias c='clear'
alias x='exit'
alias :q='exit'
alias q='exit'
alias v='vim'
alias svf='sudo vifm'
alias vf='vifm'
alias vfm='vifm'
alias f='~/sbin/fuz'
alias ff='~/sbin/fuzz'
alias reddit='rtv'
alias rdt='rtv'
alias red='rtv'
alias cp='cp -iv'
alias night='sct 4500'
alias day='sct'
alias clock='tty-clock -C 4'
alias pa='pulseaudio --start'
alias pulse='pulseaudio --start'
alias color='yad --color'
alias sc='sc -n'

########### folder locations ###########
alias msda1='pmount /dev/sda1'
alias umsda1='pumount /dev/sda1'
alias msdcard='pmount /dev/mmcblk1p3'
alias umsdcard='pumount /dev/mmcblk1p3'
alias T='cd ~/Temp && ls'
alias Tr='cd ~/Temp/Radio && ls'
alias Tm='cd ~/Temp/Music && ls'
alias Tf='cd ~/Temp/Films && ls'
alias t64='cd ~/Temp-64 && ls -a'
alias t32='cd ~/Temp-32 && ls -a'
alias r='/media && ls'
alias films='tree ~/Films'
alias docs='cd ~/Documents && ls'
alias down='cd ~/Downloads&& ls -a'
alias images='cd ~/Pictures&& ls -a'
alias pics='cd ~/Pictures&& ls -a'
alias vh='vim ~/bin/HELPFILES/helptxt.org'
alias music='cd ~/Music && ls'
alias musiclib='cd ~/Music && ls'

############ Music vids & tracks #############
alias mm='mocp'
alias mn='mocp --next && sleep 1 && mocp -i'
alias mb='mocp --previous && sleep 1 && mocp -i'
alias ms='mocp --toggle shuffle'
alias mp='mocp --toggle-pause'
alias mx='mocp -x'
alias mi='mocp -i'
alias videos='videos.sh'
alias m4aconvert='for i in *.m4a; do ffmpeg -i "$i" -b:a 196k "${i/.m4a/.opus}"; done'
alias volume='echo "ffmpeg -i input.wav -filter:a "volume=2" output.wav"'
alias bkm64='backup-music-usb64.sh'
alias bkm32='backup-music-usb32Gb.sh'
alias mpv360='mpv --ytdl-format=best[height=360]'
alias mpv480='mpv --ytdl-format=best[height=480]'
alias mpv720='mpv --ytdl-format=best[height=720]'
alias mpva='mpv --no-video'
alias m='mpv'

################ git #################
alias gs='git status'
alias gp='git push'
alias gpu='git pull'
alias gcom='git commit'
alias ga='git add'
alias gl='git ls-files'
alias glr='git ls-tree -r master --name-only'

############## System ##############
alias cp='cp -i'
alias ps1='ps -U $UID'
alias ps2='ps -U $UID f'
alias pst='ps -e --forest'
alias psth='ps -e --forest | head'
alias l='ls'
alias la='ls -a'
alias ll='ls -lh'
alias lla='ls -lha'
alias am='alsamixer'
alias t='tmux'
alias th='tmux display-popup -h 30 -w 100 -E emacsclient -t ~/bin/HELPFILES/helptxt.org'
alias ta='tmux attach'
alias dt='/usr/bin/dvtm -m ^a -M'
alias sb='source ~/.bashrc'
alias sp='source ~/.profile'
alias xd='xrdb ~/.Xdefaults'
alias xr='xrdb ~/.Xresources'
alias applications='dpkg --get-selections'
alias apps='dpkg --get-selections'
alias bat='acpi -bi'
alias battery='acpi -bi'
alias cycles='sudo tlp-stat --battery'
alias bat1='sudo tlp-stat --battery'
alias h='htop'
#alias disks='/home/adrian/sbin/disks.sh'
alias d='df -H | egrep "sd|p2|p3"'
alias df='df -H'
alias d1='/home/adrian/sbin/disks.sh'
alias du='ncdu'
alias inxi='inxi -F -c 18'
alias sys='inxi -F'
alias loc='locate -i'
alias lc='locate -i'
alias locateupdate='sudo updatedb'
alias p='ping -c 5 8.8.8.8'
alias speed='speedtest'
alias tree='tree -l'

################# Install Pacman  ###############
alias updkeys='sudo pacman-key --refresh-keys'
alias install='sudo pacman -S --color auto'
alias upd='sudo pacman -Syu --color auto'
alias yay='yay --color auto'
alias updy='yay -Syu --color auto'
alias yayu='yay -Syu --color auto'
alias paruu='paru -Syu --color auto'
alias paru='paru -S --color auto'
alias search='sudo pacman -Ss --color auto'
alias remove='sudo pacman -R --color auto'
alias removeall='sudo pacman -Rs --color auto'
alias autoremove='sudo pacman -Rs $(pacman -Qtdq)'
alias arm='sudo pacman -Rs $(pacman -Qtdq)'
alias clean='sudo pacman -Sc --color auto'
alias cleanall='sudo pacman -Scc --color auto'
alias searchfull='sudo pacman -Fy --color auto'
alias installed='sudo pacman -Qe --color auto'
alias installed1='sudo pacman -Qs --color auto'
alias ins='sudo pacman -Qe --color auto'
alias installednumber='sudo pacman -Qe --color auto | wc -l'
alias removecache='sudo pacman -Sc --color auto'
alias removeallcache='sudo pacman -Scc --color auto'
alias info='pacman -Qi'

######## make ########
alias mci='sudo make clean install'
alias smi='sudo make install'
alias mu='sudo make uninstall'
alias conf='./configure'

############ wifi nmcli ############
alias woff='nmcli radio wifi off'
alias wifioff='nmcli radio wifi off'
alias won='nmcli radio wifi on'
alias wifion='nmcli radio wifi on'
alias winfo='nmcli g'
alias wstat='nmcli g'
alias ws='nmcli g'
alias wifiinfo='nmcli g'

alias wifi='cat /proc/net/wireless'

######## Web #########
alias pipe='pipe-viewer --no-video'
alias pv='pipe-viewer --no-video'
alias pipe480='pipe-viewer --resolution 480p'
alias p48='pipe-viewer --resolution 480p'
alias p72='pipe-viewer --resolution 720p'
alias elinks='elinks https://duckduckgo.com/lite/'
alias links2='links2 https://duckduckgo.com/lite/'
alias links2g='links2 -g'
alias l2g='links2 -g'
alias tor='sudo service tor start'
alias tor1='sudo service tor stop'
alias tors='torstatus'
alias w3m='w3m https://duckduckgo.com/lite/'
alias te='torsocks elinks'
alias trd='torsocks transmission-daemon'
alias trd1='service transmission-daemon stop'
alias news='newsboat -r'
alias pod='podboat'
alias pyserver='python -m SimpleHTTPServer'
# add a port number - 8000 eg ##
alias pserver='http-server ./ -p'
# nmp server, add a port number - 8000 eg ##

## Edit Configs ##
# with emacs
alias eb='emacsclient -t ~/.bashrc'
alias ej='emacsclient -t ~/.jwmrc'
alias ex='emacsclient -t ~/.Xdefaults'
alias exd='emacsclient -t ~/.Xdefaults'
alias exr='emacsclient -t ~/.Xresources'
alias ee='emacsclient -t ~/.emacs.d/myinit.org'
# with vim
alias vb='vim ~/.bashrc'
alias vj='vim ~/.jwmrc'
alias vxd='vim ~/.Xdefaults'
alias vxr='vim ~/.Xresources'
alias ve='vim ~/.emacs'

## My Programs ##
alias col='zathura 256Colors.pdf'
alias dragon='dragon-drag-and-drop'
alias drag='dragon-drag-and-drop'
alias drop='dragon-drag-and-drop'
alias blender='/home/adrian/programs/blender/blender'
#emacs
alias emacsgui='TERM=xterm-256color emacsclient -tfs'
alias ed='emacs --daemon'
alias emt='emacsclient -nw'
alias e='emacsclient -t'
alias bl='bleachbit --preset -p'
alias bl1='bleachbit --preset -c'
alias n='nnn -l'
#alias n='noice'
alias xcalc='xcalc -stipple'
#alias films='/home/adrian/sbin/films.sh'
# alias i='sxiv -f -t -r ./*'
alias i='/home/adrian/bin/sxiv-open'
alias s='/home/adrian/bin/sxiv-open'
alias img='/home/adrian/bin/sxiv-open'
alias imgs='sxiv -f -t -r'
alias ir='jalv.gtk http://factorial.hu/plugins/lv2/ir'
alias mixer='ncpamixer'
alias sl='~/programs/firestorm/firestorm'
alias qcad='/home/adrian/programs/qcad/qcad'
alias db='dosbox -conf'
alias op='opustags'
alias pdf='zathura'
alias z='zathura'
## archives zip tar etc
alias untar='tar xvf'
#alias unzip='unzip -xvf'
## curl commands
## weather add eg. /london
alias weather='curl wttr.in'
alias weatherl='curl wttr.in/London'
## ip address
alias ip='curl ifconfig.co'
##location or /country
alias where='curl ifconfig.co/city'
## tech news - add additional search - eg. /trump
alias tech='curl getnews.tech'

## You Tube ##
alias ytd='youtube-dl'
alias ytdm='youtube-dl --add-metadata'
alias ytdmp3='youtube-dl -x --audio-format mp3 --add-metadata'
alias ym='youtube-dl -x --audio-format vorbis -f bestaudio --add-metadata'
alias ytm='youtube-dl -i -f bestaudio -x --add-metadata'
alias ytm2='youtube-dl -i -f 251 --add-metadata'
alias yta='youtube-viewer --video-player=mplayer -novideo'
alias ytv='pipe-viewer'
alias pv='pipe-viewer'
alias viewer='pipe-viewer'
alias mpl='mplayer'
alias infowars='mpv --no-video http://50.7.69.18:80/alexjonesshow'
alias alex='mpv --no-video http://50.7.69.18:80/alexjonesshow'

# Audio conversion
alias trim='echo "ffmpeg -i input.opus -ss 2 -c copy output.opus"'
alias convert1='echo "ffmpeg -i name.m4a -b:a 196k output.opus"'
alias convert='echo "ffmpeg -i name.m4a -c copy output.opus"'

## radio
alias infowars='mpv --no-video http://50.7.69.18:80/alexjonesshow'
alias r3='~/sbin/radio3'
alias r4='~/sbin/radio4fm.sh'
alias r4lw='~/sbin/radio4lw.sh'
alias rw='~/sbin/radio-world-service'
alias rtalk='~/sbin/talkradio'
alias rsport='~/sbin/talksport'

## Help ##
alias eh='emacsclient -t ~/bin/HELPFILES/helptxt.org'
alias helpme='emacsclient -t ~/bin/HELPFILES/helptxt.org'
alias myhelp='vim ~/bin/HELPFILES/helptxt.org'
alias keys='vim ~/bin/HELPFILES/allkeys.txt'
alias bashhelp='vim ~/bin/HELPFILES/bash-help'
alias helpkeys='vim ~/bin/HELPFILES/allkeys.txt'

## PATHS  ##
WWW_HOME='duckduckgo.com/lite'
export WWW_HOME
#export EDITOR='vim'
#export BROWSER='librewolf'
# GPG_TTY=`tty`
# export GPG_TTY
export PATH="$HOME/bin:$HOME/sbin:$HOME/.local/bin:$PATH"
#[ -f ~/.fzf.bash ] && source ~/.fzf.bash
## Install, Make and Apt-Get ##
#alias upgrade='sudo apt'
#alias upg='sudo apt upgrade'
#alias update='sudo apt update'
#alias upd='sudo apt update'
#alias autoclean='sudo apt autoclean'
#alias aedit='sudo apt edit-sources'
#alias acl='sudo apt autoclean'
#alias check='sudo apt-get check'
#alias asearch='apt search'
##alias alist='apt list'
#alias clean='sudo apt clean'
#alias install='sudo apt install'
#alias installed='apt list --installed'
#alias anumber='apt list --installed | wc -l'
#alias remove='sudo apt remove'
#alias aptdetails='apt show' #packagename#
#alias adetails='apt show' #packagename#
##alias packagesize='package-size.sh'
#alias apthistory='less /var/log/apt/history.log | tail -n 200'
#alias ahistory='less /var/log/apt/history.log | tail -n 200'
#alias hold='sudo apt-mark hold'
#alias autoremove='sudo apt-get autoremove'
#alias arm='sudo apt-get autoremove'
#alias apurge='sudo apt purge'
#alias synaptic='sudo synaptic'
#alias ideb='sudo dpkg -i'
#alias fix='sudo apt --fix-broken install'
#alias syn='sudo synaptic'
#alias ah='cat /var/log/dpkg.log | tail -80 | less'
#alias ach='grep -i "Commandline" /var/log/apt/history.log | tail -40'
