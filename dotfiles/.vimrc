" Pevsner vimrc
" General
" This line should not be removed as it ensures that various options are
" properly set to work with the Vim-related packages available in Debian.
" runtime! debian.vim
" Uncomment the next line to make Vim more Vi-compatible
" NOTE: debian.vim sets 'nocompatible'.  Setting 'compatible' changes numerous
" options, so any other options should be set AFTER setting 'compatible'.
" set nocompatible
" syntax enable
" Colour Theme
" set t_Co=16
set term=xterm-256color
set background=light
colorscheme mine
" Misc
" set relativenumber "Turn on numbers at startup"
" Toggle line numbers from none at all
" to relative numbering with current line number
" allow backspacing over everything in insert mode
set backspace=indent,eol,start
set history=50          " keep 50 lines of command line history
set ruler               " show the cursor position all the time
" The following are commented out as they cause vim to behave a lot
" differently from regular Vi. They are highly recommended though.
set showcmd		" Show (partial) command in status line.
set showmatch		" Show matching brackets.
set ignorecase		" Do case insensitive matching
set smartcase		" Do smart case matching
set autowrite		" Automatically save before commands like :next and :make
set hidden		" Hide buffers when they are abandoned
set mouse=a		" Enable mouse usage (all modes)
set cursorline          " highlight current line
" set cursorcolumn        " highlight column
set wildmenu            " visual autocomplete for command menu
set lazyredraw          " redraw only when we need to.
set incsearch           " search as characters are entered
set hlsearch            " highlight matches
"These next three lines are for the fuzzy search:
set path+=**          "Search all subdirectories and recursively
" toggle toolbar in gvim "
" set guioptions-=T
set nu!
"augroup AutoSaveFolds
"  autocmd!
  " view files are about 500 bytes
  " bufleave but not bufwinleave captures closing 2nd tab
  " nested is needed by bufwrite* (if triggered via other autocmd)
"  autocmd BufWinLeave,BufLeave,BufWritePost ?* nested silent! mkview!
"  autocmd BufWinEnter ?* silent loadview
"augroup end
""augroup remember_folds
""  autocmd!
""  autocmd BufWinLeave * mkview
""  autocmd BufWinEnter * silent! loadview
""augroup END
au BufWinLeave * mkview
au BufWinEnter * silent loadview

set viewoptions=folds,cursor
set sessionoptions=folds
packloadall
silent! helptags ALL
" Keys - Aliases
" page up , page down"
nnoremap <PageUp> <C-u>
nnoremap <PageDown> <C-d>
" Tabs
ca tn tabnew
ca nt tabnew
ca th tabp
ca tl tabn
ca co ColorToggle
ca go Goyo
ca sp setlocal spell! spelllang=en_gb
"To create a new tab
nnoremap <C-t> :tabnew<Space>
inoremap <C-t> <Esc>:tabnew<Space>
" cursor shape
let &t_SI.="\e[5 q"
let &t_SR.="\e[4 q"
let &t_EI.="\e[1 q"
" advanced customization using autoload functions
set laststatus=2
" Status line colour based on mode
 if version >= 700
	au InsertEnter * hi StatusLine term=none ctermfg=231 ctermbg=13 

	au InsertLeave * hi StatusLine term=none ctermfg=231  ctermbg=61  gui=bold,
     endif
set noshowmode
" Keys
map <F4> :Goyo<cr>
map <F5> :set invnumber<CR>      
map <F6> :colorscheme dark<cr>
map <F7> :colorscheme mine-green <cr>
nmap <F1> :setlocal spell! spelllang=en_gb<CR>
"netrw
" Per default, netrw leaves unmodified buffers open.  This autocommand
" deletes netrw's buffer once it's hidden (using ':q;, for example)
autocmd FileType netrw setl bufhidden=delete  " or use :qa!
" INDENTATION "
au BufNewFile,BufRead *.py
    \ set tabstop=4
    \ set softtabstop=4
    \ set shiftwidth=4
    \ set textwidth=79
    \ set expandtab
    \ set autoindent
    \ set fileformat=unix
au BufNewFile,BufRead *.js, *.html, *.css:
    \ set tabstop=2
    \ set softtabstop=2
    \ set shiftwidth=2
"
"au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/
set encoding=utf-8
" GnuPG Extensions "
""""""""""""""""""""

" Tell the GnuPG plugin to armor new files.
let g:GPGPreferArmor=1

" Tell the GnuPG plugin to sign new files.
let g:GPGPreferSign=1

augroup GnuPGExtra
" Set extra file options.
    autocmd BufReadCmd,FileReadCmd *.\(gpg\|asc\|pgp\) call SetGPGOptions()
" Automatically close unmodified files after inactivity.
    autocmd CursorHold *.\(gpg\|asc\|pgp\) quit
augroup END
" FOLDS
    set foldlevel=0
    set modelines=1
function SetGPGOptions()
" Set updatetime to 1 minute.
    set updatetime=60000
" Fold at markers.
    set foldmethod=marker
" Automatically close all folds.
    set foldclose=all
" Only open folds with insert commands.
    set foldopen=insert
endfunction
" vim:foldmethod=marker:foldlevel=0
"Section Name {{{
set number "This will be folded
" }}}
